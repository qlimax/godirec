<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en">
<context>
    <name>Dialog</name>
    <message>
        <location filename="godirec/data/ui/dialog.ui" line="14"/>
        <source>erzeuge Projekt</source>
        <translation>Erzeuge Projektn</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/dialog.ui" line="46"/>
        <source>Projekt Name:</source>
        <translation>Projekt Name:</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/dialog.ui" line="66"/>
        <source>Erzeugen</source>
        <translation>Erzeugen</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/settings.ui" line="20"/>
        <source>Einstellungen</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/settings.ui" line="48"/>
        <source>Export Settings</source>
        <translation>Export Einstellungen</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/settings.ui" line="56"/>
        <source>Standart ausgabe Formate:</source>
        <translation>Standard Ausgabe Formate:</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/settings.ui" line="66"/>
        <source>Autovervollständigung</source>
        <translation type="obsolete">Autovervollständigung</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/settings.ui" line="83"/>
        <source>Hinzufügen</source>
        <translation type="obsolete">Hinzufügen</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/settings.ui" line="90"/>
        <source>Entfernen</source>
        <translation>Entfernen</translation>
    </message>
</context>
<context>
    <name>GodiRec</name>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="15"/>
        <source>GodiRec - erzeuge neues Projekt</source>
        <translation>Godirec - Erzeuge neues Projekt</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="37"/>
        <source>strg+J</source>
        <translation>Strg+J</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="47"/>
        <source>Ctrl+J</source>
        <translation>Ctrl+J</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="54"/>
        <source>strg+K</source>
        <translation>Strg+K</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="64"/>
        <source>Ctrl+K</source>
        <translation>Ctrl+K</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="71"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;beginnt einen neuen Track.&lt;/p&gt;&lt;p&gt;strg+L&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;beginnt einen neuen Track&lt;/p&gt;&lt;p&gt;Strg+L&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="81"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="105"/>
        <source>-- / --</source>
        <translation></translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="145"/>
        <source>Titel:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="155"/>
        <source>Artist:</source>
        <translation>Künstler:</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="165"/>
        <source>Album:</source>
        <translation>Album:</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="175"/>
        <source>Genre:</source>
        <translation>Genre:</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="185"/>
        <source>Jahr:</source>
        <translation>Jahr:</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="202"/>
        <source>yyyy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="220"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="231"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="234"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="239"/>
        <source>Neues Projekt</source>
        <translation>Neues Projekt</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="242"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="247"/>
        <source>Altes Projekt</source>
        <translation>Altes Projekt</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="250"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="259"/>
        <source>Einstellungen</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="262"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
</context>
<context>
    <name>GodiRecWindow</name>
    <message>
        <location filename="godirec/gui.py" line="286"/>
        <source>Converting Track</source>
        <translation>Konvertiere Track</translation>
    </message>
    <message>
        <location filename="godirec/gui.py" line="356"/>
        <source>Stream beenden</source>
        <translation>Stream beenden</translation>
    </message>
    <message>
        <location filename="godirec/gui.py" line="357"/>
        <source>Um das Programm zu schlieÃen,
mÃ¼ssen sie zuerst den Stream beenden</source>
        <translation>Um das Programm zu schließen,
müssen Sie zuerst den Stream beenden</translation>
    </message>
    <message>
        <location filename="godirec/gui.py" line="364"/>
        <source>Die Tracks mÃ¼ssen erst fertig konvertiert sein, bevor das Programm geschlossen werden kann!</source>
        <translation>Die Tracks müssen erst fertig konvertiert werden, bevor das Programm geschlossen werden kann!</translation>
    </message>
</context>
<context>
    <name>PathDialog</name>
    <message>
        <location filename="godirec/gui.py" line="164"/>
        <source>Neues Projekt erzeugen in:</source>
        <translation>Neues Projekt erzeugen in:</translation>
    </message>
</context>
</TS>
