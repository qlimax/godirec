<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>Dialog</name>
    <message>
        <location filename="godirec/data/ui/dialog.ui" line="14"/>
        <source>erzeuge Projekt</source>
        <translation>create project</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/dialog.ui" line="46"/>
        <source>Projekt Name:</source>
        <translation>Project Name:</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/dialog.ui" line="66"/>
        <source>Erzeugen</source>
        <translation>Create</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/settings.ui" line="20"/>
        <source>Einstellungen</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/settings.ui" line="48"/>
        <source>Export Settings</source>
        <translation>Export Settings</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/settings.ui" line="56"/>
        <source>Standart ausgabe Formate:</source>
        <translation>Standard export formats:</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/settings.ui" line="66"/>
        <source>Autovervollständigung</source>
        <translation>Autocomplition</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/settings.ui" line="83"/>
        <source>Hinzufügen</source>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/settings.ui" line="90"/>
        <source>Entfernen</source>
        <translation>Remove</translation>
    </message>
</context>
<context>
    <name>GodiRec</name>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="15"/>
        <source>GodiRec - erzeuge neues Projekt</source>
        <translation>GodiRec - create new project</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="37"/>
        <source>strg+J</source>
        <translation>Ctrl+J</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="47"/>
        <source>Ctrl+J</source>
        <translation></translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="54"/>
        <source>strg+K</source>
        <translation>Ctrl+K</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="64"/>
        <source>Ctrl+K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="71"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;beginnt einen neuen Track.&lt;/p&gt;&lt;p&gt;strg+L&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>start new Track.\n Ctrl+L</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="81"/>
        <source>Ctrl+L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="105"/>
        <source>-- / --</source>
        <translation></translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="145"/>
        <source>Titel:</source>
        <translation>Title:</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="155"/>
        <source>Artist:</source>
        <translation>Artist:</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="165"/>
        <source>Album:</source>
        <translation>Album:</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="175"/>
        <source>Genre:</source>
        <translation>Genre:</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="185"/>
        <source>Jahr:</source>
        <translation>Year:</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="202"/>
        <source>yyyy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="220"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="231"/>
        <source>Exit</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="234"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="239"/>
        <source>Neues Projekt</source>
        <translation>New Project</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="242"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="247"/>
        <source>Altes Projekt</source>
        <translation>Old Project</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="250"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="259"/>
        <source>Einstellungen</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="godirec/data/ui/godi_rec.ui" line="262"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GodiRecWindow</name>
    <message>
        <location filename="godirec/gui.py" line="286"/>
        <source>Converting Track</source>
        <translation>Converting Track</translation>
    </message>
    <message>
        <location filename="godirec/gui.py" line="356"/>
        <source>Stream beenden</source>
        <translation>Close stream</translation>
    </message>
    <message>
        <location filename="godirec/gui.py" line="357"/>
        <source>Um das Programm zu schlieÃen,
mÃ¼ssen sie zuerst den Stream beenden</source>
        <translation>To close the program, you have to close the stream first.</translation>
    </message>
    <message>
        <location filename="godirec/gui.py" line="364"/>
        <source>Die Tracks mÃ¼ssen erst fertig konvertiert sein, bevor das Programm geschlossen werden kann!</source>
        <translation>Before the program can be closed, the tracks need to be finished converting</translation>
    </message>
</context>
<context>
    <name>PathDialog</name>
    <message>
        <location filename="godirec/gui.py" line="164"/>
        <source>Neues Projekt erzeugen in:</source>
        <translation>Create new project:</translation>
    </message>
</context>
</TS>
